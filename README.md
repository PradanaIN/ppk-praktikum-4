# PPK-Praktikum 4 : RESTful Web Service

## Identitas

```
Nama : Novanni Indi Pradana
NIM  : 222011436
Kelas: 3SI3

```

## Deskripsi

Web service adalah suatu metode komunikasi antar dua perangkat elektronik lewat world wide web. Web service membantu kita dalam mengkonversi aplikasi kita menjadi aplikasi berbasis web. Saat ini terdapat 2 macam web service yaitu web service yang menggunakan SOAP dan WSDL, dan web service yang menggunakan REST, atau lebih dikenal dengan RESTful web service. Pada praktikum kali ini kita akan mempraktekkan pembuatan RESTful web service.

## Live Demo

https://ppk-api-documentation.herokuapp.com/

## Localhost Deployment

![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-4/-/raw/main/screenshot/modul4(20).png)

## Kegiatan Praktikum

## 1. Menampilkan Semua Data
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-4/-/raw/main/screenshot/modul4%20(10).png)

## 2. Menampilkan Data Spesifik
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-4/-/raw/main/screenshot/modul4%20(11).png)

## 3. Mengubah Data
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-4/-/raw/main/screenshot/modul4%20(12).png)
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-4/-/raw/main/screenshot/modul4%20(13).png)
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-4/-/raw/main/screenshot/modul4%20(14).png)
  
## 4. Menambahkan Data
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-4/-/raw/main/screenshot/modul4%20(15).png)  
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-4/-/raw/main/screenshot/modul4%20(16).png)  
  
## 5. Menghapus Data
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-4/-/raw/main/screenshot/modul4%20(17).png)  
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-4/-/raw/main/screenshot/modul4%20(18).png)  
